import { Sequelize } from "sequelize";

const db = new Sequelize("nodejs_pagination_dbase", "root", "root", {
  host: "localhost",
  dialect: "mysql",
});

export default db;
